#!/usr/bin/ruby -w
require "uri"
require "net/http"
require "json"
require "optparse"

options = {}
OptionParser.new do |opts|
	opts.banner = "Usage: weather.rb [options]"

	opts.on("-f", "--forecast", "Show weather forecast") do |f|
		options[:forecast] = "fc"
	end

	opts.on("-h", "--help", "Enter city name and a country code")
end.parse!

# Get the city name
cityName = ARGV[0]
countryCode = ARGV[1]

special = "?<>',[]}{=-)(*&^%$#`~"
regex = /[#{special.gsub(/./){|char| "\\#{char}"}}]/

# Abort if an argument has not been entered
if (ARGV.size < 2 || cityName =~ regex || countryCode =~ regex)
	abort("You need to enter a city name and a country code.")
end

# Create the necessary URI
uri = URI.parse("http://api.openweathermap.org/data/2.5/forecast?q=" + cityName + "," + countryCode + "&APPID=0d62625c8a65a74e43385711dfca38bc" + "&units=metric")

# Make the request
http = Net::HTTP.new(uri.host, uri.port)
request = Net::HTTP::Get.new(uri.request_uri)

# Get the response and parse
response = http.request(request)
$result = JSON.parse(response.body)

# Check if the result is successful
if $result["cod"] != "200"
  	abort("Error with connection!")
end

# Required result variables
$date = Array.new(3)
$currentTemp = Array.new(3)
$minTemp = Array.new(3)
$maxTemp = Array.new(3)

# Store data method
def storeData(totalDays)
	i = 0
	days = $result["list"]
	while i<totalDays do
  		days.each do |iterate|
  			$date[i] = iterate["dt_txt"]
  			$currentTemp[i] = iterate["main"]["temp"]
  			$minTemp[i] = iterate["main"]["temp_min"]
  			$maxTemp[i] = iterate["main"]["temp_max"]
   			i=i+1
  		end	
	end
end

# Print forecast results method
def printResultsFc
	# Print the results
	printf "| " + "%12s %7s", "Today", " |"
	printf "%15.10s %5s", $date[1].to_s, " |"
	printf "%15.10s %5s\n", $date[2].to_s, " |"
	printf "| " + "%-12s %.2f ", "Current:", $currentTemp[0].to_s
	printf "| " + "%-12s %.2f ", "Current:", $currentTemp[1].to_s
	printf "| " + "%-12s %.2f |\n", "Current:", $currentTemp[2].to_s
	printf "| " + "%-12s %.2f ", "Minimum:", $minTemp[0].to_s
	printf "| " + "%-12s %.2f ", "Minimum:", $minTemp[1].to_s
	printf "| " + "%-12s %.2f |\n", "Minimum:", $minTemp[2].to_s
	printf "| " + "%-12s %.2f ", "Maximum:", $maxTemp[0].to_s
	printf "| " + "%-12s %.2f ", "Maximum:", $maxTemp[1].to_s
	printf "| " + "%-12s %.2f |\n", "Maximum:", $maxTemp[2].to_s
end

# Print single day results method
def printResultsSgl
	# Print the results
	printf "| " + "%12s %7s\n", "Today", " |"
	printf "| " + "%-12s %.2f |\n", "Current:", $currentTemp[0].to_s
	printf "| " + "%-12s %.2f |\n", "Minimum:", $minTemp[0].to_s
	printf "| " + "%-12s %.2f |\n", "Maximum:", $maxTemp[0].to_s
end

# The logic - call the appropriate methods depending on input
if options[:forecast] == "fc"
  	storeData(3)
  	printResultsFc
else
	storeData(1)
	printResultsSgl
end
